<?php
namespace clases;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Conexion
 *
 * @author chema
 */
class Conexion {
    
    private $enlace;
    private $resultado;
    public $salida;
    
    //creamos objeto tipo conexion
    public function __construct($a=[]) {
        $b=[
            'host'=>'127.0.0.1',
            'user'=>'root',
            'password'=>''
        ];
        $a= array_merge($b,$a);
        $this->enlace = new \mysqli($a['host'],$a['user'],$a['password']);
        $this->resultado = $this->enlace->query("show databases");
        $this->salida  = $this->resultado->fetch_all();
    }
    
    public function consulta($argumento = 'show databases'){
        
        $this->resultado = $this->enlace->query($argumento);   
            if(gettype($this->resultado) == 'object'){
            $this->verConsulta();
        }
    }
    
    public function verConsulta(){
        $this->salida  = $this->resultado->fetch_all();
    }

    public function baseDatos($bd){
        $this->enlace->select_db($bd);
    }
}
