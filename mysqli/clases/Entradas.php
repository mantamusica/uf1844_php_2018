<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace clases;

/**
 * Description of Entradas
 *
 * @author chema
 */
class Entradas {
    public $puesto;
    public $nombre;
    public $apellidos;
    
    public function __toString() {
        return "el registro es ". $this->nombre.' - '.$this->apellidos;
    }

}
