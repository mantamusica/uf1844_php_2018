<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1 de la hoja de Ejercicios 1 de PHP</title>
    </head>
    <body>
        En este ejercicio vamos a escribir un texto en la página web a través de php.
        <?php
        // esto es un comentario

        echo "Este texto esta escrito desde el Script de php<hr>";

        /* Esto es un comentario
         * pero con la diferencia de que es de varias lineas.
         */

        echo "Este texto también va dentro del script.";
        ?>
        <hr>
        Escrito en html normal.
        <hr>
        <?php
        #####################################################################
        ################Este comentario es una linea#########################
        #####################################################################
        #En una página podemos colocar tantos scripts de php como desees.

        print("Otra forma de poner comentarios");
        ?>
    </body>
</html>
