<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio2 de la hoja de ejercicios 1</title>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#999;margin:0px auto;}
            .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;border-top-width:1px;border-bottom-width:1px;}
            .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;border-top-width:1px;border-bottom-width:1px;}
            .tg .tg-vn4c{background-color:#D2E4FC}
        </style>
    </head>
    <body>

        <table class="tg">
            <tr>
                <th class="tg-031e"><?php echo 'Texto en php'; ?></th>
                <th class="tg-031e">Texto directamente en html</th>
            </tr>
            <tr>
                <td class="tg-031e"><?php print ('Texto en php'); ?></td>
                <td class="tg-vn4c"><?php echo 'Texto en php'; ?></td>
            </tr>
        </table>
    </body>
</html>
