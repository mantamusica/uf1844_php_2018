<?php
/* Listar Base Datos para crear una nueva. */
if (isset($_POST['si']) && isset($_SESSION['informacion']['crearBBDD'])) {
    ?>
    <h1>Listado de Bases de Datos:</h1>
    <form class="form-inline" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
        <div class="form-group">
            <?php
            $databases = new Conexion();
            $databases->mostrarBBDD();
            $databases->consulta();
            $databases->listar_registros();
            ?>
            <input type="text" name="baseDatos" placeholder="Nombre de la BBDD."/><br>
            <input type="submit" name="aceptar" value="Aceptar"/>
            <input type="submit" name="volver" value="Volver"/>
        </div>
    </form>
    <?php
}
/* Proceso para crear BBDD nueva. */
if (isset($_POST['aceptar']) && isset($_SESSION['informacion']['crearBBDD'])) {
    $baseDatos = $_POST['baseDatos'];
    $conexionCrear = new Conexion();
    $conexionCrear->crearBBDD($baseDatos);
    $conexionCrear->consulta();
    echo "<hr>Base de Datos $baseDatos creada.";
    echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST"><input type="submit" name="inicio" value="Volver"/></form>';
}
/* Listar Base Datos para escoger una BBDD. */
if (isset($_POST['si']) && isset($_SESSION['informacion']['borrarBBDD'])) {
    ?>
    <h1>Listado de Bases de Datos para Borrar:</h1>
    <form class="form-inline" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
        <div class="form-group">
            <?php
            $databases = new Conexion();
            $databases->mostrarBBDD();
            $databases->consulta();
            $databases->listar_bbdd_radio();
            ?>
            <br>
            <input type="submit" name="aceptar" value="Aceptar"/>
            <input type="submit" name="volver" value="Volver"/>
        </div>
    </form>
    <?php
}
/* Proceso para borrar BBDD escogida. */
if (isset($_POST['aceptar']) && isset($_SESSION['informacion']['borrarBBDD'])) {
    foreach ($_POST as $key => $value) {
        if ($value == 'on') {
            $bbddBorrar = $key;
        }
    }
    $conexionBorrar = new Conexion('127.0.0.1', 'root', '', $bbddBorrar);
    $conexionBorrar->borrarBBDD($bbddBorrar);
    $conexionBorrar->consulta();
    echo "<hr>Base de Datos $bbddBorrar borrada.";
    echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST"><input type="submit" name="inicio" value="Volver"/></form>';
}
if (isset($_POST['si']) && isset($_SESSION['informacion']['editarBBDD'])) {
    ?>
    <h1>Listado de Bases de Datos para Edición:</h1>
    <form class="form-inline" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
        <div class="form-group">
            <?php
            $databases = new Conexion();
            $databases->mostrarBBDD();
            $databases->consulta();
            $databases->listar_bbdd_radio();
            ?>
            <br>
            <input type="submit" name="aceptar" value="Aceptar"/>
            <input type="submit" name="volver" value="Volver"/>
        </div>
    </form>
    <?php
}
/* Proceso para editar nombre BBDD escogida. */
if (isset($_POST['aceptar']) && isset($_SESSION['informacion']['borrarBBDD'])) {
    foreach ($_POST as $key => $value) {
        if ($value == 'on') {
            $bbddBorrar = $key;
        }
    }
    $conexionBorrar = new Conexion('127.0.0.1', 'root', '', $bbddBorrar);
    $conexionBorrar->borrarBBDD($bbddBorrar);
    $conexionBorrar->consulta();
    echo "<hr>Base de Datos $bbddBorrar borrada.";
    echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST"><input type="submit" name="inicio" value="Volver"/></form>';
}
if (isset($_POST['si']) && isset($_SESSION['informacion']['listarBBDD'])) {
    ?>
    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
        <?php
        $databases = new Conexion();
        $databases->mostrarBBDD();
        $databases->consulta();
        $databases->listar_registros();
        ?>
        <input type="submit" name="volver" value="Volver"/>
    </form>
    <?php
}
if (isset($_POST['si']) && isset($_SESSION['informacion']['listar'])) {
    ?>
    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
        <?php
        $databases = new Conexion();
        $databases->mostrarBBDD();
        $databases->consulta();
        $databases->listar_registros();
        echo 'Escoge una bbdd del listado que desees borrar:';
        ?>
        <input type="text" name="baseDatos" placeholder="Nombre de la BBDD." required=""/><br>
        <input type="text" name="tabla" placeholder="Nombre de la tabla." required=""/><br>
        <input type="submit" name="aceptar" value="Aceptar"/>
        <input type="submit" name="volver" value="Volver"/>
    </form>
    <?php
}
if (isset($_POST['si']) && isset($_SESSION['informacion']['borrarRegistros'])) {
    ?>
    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
        <?php
        $databases = new Conexion();
        $databases->mostrarBBDD();
        $databases->consulta();
        $databases->listar_registros();
        echo 'Escoge una bbdd del listado que desees borrar:';
        ?>
        <input type="text" name="baseDatos" placeholder="Nombre de la BBDD." required=""/><br>
        <input type="submit" name="aceptar" value="Aceptar"/>
        <input type="submit" name="volver" value="Volver"/>
    </form>
    <?php
}


if (isset($_POST['aceptar']) && isset($_SESSION['informacion']['insertar'])) {
    $baseDatos = $_POST['baseDatos'];
    $tablaBBDD = $_POST['tabla'];
    $argumento1 = $_POST['arg1'];
    $argumento2 = $_POST['arg2'];
    $argumento3 = $_POST['arg3'];
    $conexionInsertar = new Conexion('127.0.0.1', 'root', '', $baseDatos);
    $conexionInsertar->insertarDatos($tablaBBDD, $argumento1, $argumento2, $argumento3);
    $conexionInsertar->consulta();
    echo "<hr>Insercción de datos en BBDD $baseDatos realizada.";
    echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST"><input type="submit" name="inicio" value="Volver"/></form>';
}
if (isset($_POST['aceptar']) && isset($_SESSION['informacion']['listar'])) {
    $baseDatos = $_POST['baseDatos'];
    $tablaBBDD = $_POST['tabla'];
    $conexionListar = new Conexion('127.0.0.1', 'root', '', $baseDatos);
    $conexionListar->seleccionar();
    $conexionListar->setCampos("*");
    $conexionListar->setTabla($tablaBBDD);
    $conexionListar->consulta();
    $conexionListar->listar_registros();
    echo "<hr>Listado de datos en BBDD $baseDatos en su tabla $tablaBBDD.";
    echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST"><input type="submit" name="inicio" value="Volver"/></form>';
}
if (isset($_POST['aceptar']) && isset($_SESSION['informacion']['borrarRegistros'])) {
    $baseDatos = $_POST['baseDatos'];
    $tablaBBDD = $_POST['tabla'];
    $conexionListar = new Conexion('127.0.0.1', 'root', '', $baseDatos);
    $conexionListar->seleccionar();
    $conexionListar->setCampos("*");
    $conexionListar->setTabla($tablaBBDD);
    $conexionListar->consulta();
    $conexionListar->listar_registros();
    echo "<hr>Listado de datos en BBDD $baseDatos en su tabla $tablaBBDD.";
    echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST"><input type="submit" name="inicio" value="Volver"/></form>';
}
?>