<nav class="navbar navbar-default" role="navigation">
    <!-- El logotipo y el icono que despliega el menú se agrupan
         para mostrarlos mejor en los dispositivos móviles -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-ex1-collapse">
            <span class="sr-only">Desplegar navegación</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Práctica 12</a>
    </div>

    <!-- Agrupar los enlaces de navegación, los formularios y cualquier
         otro elemento que se pueda ocultar al minimizar la barra -->
    <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Práctica 12</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li class="nav-item dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Crear <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="index.php?crearBBDD">Base de Datos</a></li>
                        <li><a href="index.php?crearTablas">Tablas</a></li>
                        <li><a href="index.php?crearRegistros">Registros</a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Listar <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="index.php?listarBBDD">Base de Datos</a></li>
                        <li><a href="index.php?listarTables">Tablas</a></li>
                        <li><a href="index.php?listarRegistros">Registros</a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Editar <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="index.php?editarBBDD">Base de Datos</a></li>
                        <li><a href="index.php?editarTablas">Tablas</a></li>
                        <li><a href="index.php?editarRegistros">Registros</a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Borrar <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="index.php?borrarBBDD">Base de Datos</a></li>
                        <li><a href="index.php?borrarTablas">Tablas</a></li>
                        <li><a href="index.php?borrarRegistros">Registros</a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?buscarRegistros">Buscar Registros</a>
                </li>
            </ul>


        </div>
    </nav>
    <div class="container">
