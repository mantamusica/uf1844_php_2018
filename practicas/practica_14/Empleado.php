<?php

class Empleado extends Persona{
    protected $sueldo;
    
    function __construct($nombre, $edad, $sueldo) {
        parent::__construct($nombre, $edad);
        $this->sueldo = $sueldo;
    }
    
    function getSueldo() {
        return $this->sueldo;
    }

    function setSueldo($sueldo) {
        $this->sueldo = $sueldo;
    }

    public function render(){
        echo $this->getNombre().' - '.$this->getEdad().' - '.$this->getSueldo();
        echo '<br>';
    }


}
