<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Television
 *
 * @author chema
 */
class Television extends Electrodomestico{
    private $resolucion;
    private $sintonizador;
    
    function getResolucion() {
        return $this->resolucion;
    }

    function getSintonizador() {
        return $this->sintonizador;
    }

        function __construct() {
        
        // asumo que no paso ningun argumento
        $this->resolucion = 20;
        $this->sintonizador = FALSE;
        $this->__construct0();
        // compruebo argumentos pasados
        $params = func_get_args();
        $num_params = func_num_args();
        $funcion_constructor = '__construct' . $num_params;
        if (method_exists($this, $funcion_constructor)) {
            call_user_func_array(array($this, $funcion_constructor), $params);
        }
    }
    
        public function __construct2($precioBase, $peso) {
        $this->setPrecioBase($precioBase);
        $this->setPeso($peso);
    }
    
        function __construct3($resolucion, $sintonizador) {
        $this->resolucion = $resolucion;
        $this->sintonizador = $sintonizador;
        parent::__construct0();
        
    }
    
        public function precioFinalTelevision() {
        if(($this->resolucion > 40) && ($this->sintonizador == TRUE)){
            $precioFinal = ($this->precioFinal() * 1.3)+50;
            echo '<br>El precio de la televisión '.$precioFinal;
        }else if (($this->resolucion > 40) && ($this->sintonizador == FALSE)){
            $precioFinal = $this->precioFinal() * 1.3;
            echo '<br>El precio de la televisión '.$precioFinal;
        }else if (($this->resolucion <= 40) && ($this->sintonizador == TRUE)){
            $precioFinal = $this->precioFinal() + 50;
            echo '<br>El precio de la televisión '.$precioFinal;
        }else{
            $precioFinal = $this->precioFinal();
            echo '<br>El precio de la televisión '.$precioFinal;
        }
}

}
