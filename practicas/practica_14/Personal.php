<?php

class Personaje {

    protected $nombre = "";
    protected $edad = 0;
    protected $dni;
    protected $sexo = 'hombre';
    protected $peso = 0;
    protected $altura = 0;

    function __construct() {

    }

    function __constructAlgo($nombre, $edad, $sexo) {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->sexo = $sexo;
    }

    function __constructTodos($nombre, $edad, $dni, $sexo, $peso, $altura) {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->dni = $dni = generarDni();
        $this->sexo = $sexo;
        $this->peso = $peso;
        $this->altura = $altura;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getEdad() {
        return $this->edad;
    }

    function getSexo() {
        return $this->sexo;
    }

    function getPeso() {
        return $this->peso;
    }

    function getAltura() {
        return $this->altura;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    function setPeso($peso) {
        $this->peso = $peso;
    }

    function setAltura($altura) {
        $this->altura = $altura;
    }

    public function calcularIMC() {
        $valorIMC = $this->peso / (pow($this->altura, 2));
        if ($valorIMC < 20) {
            $valor = -1;
        } else if ($valorIMC >= 20 || $valorIMC <= 25) {
            $valor = 0;
        } else {
            $valor = 1;
        }
        return $valor;
    }

    public function esMayorDeEdad() {
        if ($this->edad > 17) {
            return true;
        }
    }

    public function comprobarSexo() {
        if ($this->sexo != 'hombre' && $this->sexo != 'mujer') {
            $this->sexo = 'H';
        } else if ($this->sexo == 'hombre') {
            $this->sexo = 'H';
        } else if ($this->sexo == 'mujer') {
            $this->sexo = 'M';
        }
    }

    public function toString() {
        $objectToString = $this->nombre . '' . $this->edad . '' . $this->sexo . '' . $this->peso . '' . $this->altura . '' . calcularIMC() . '' . esMayorDeEdad() . '' . $this->dni;
        ;
        echo $objectToString;
    }

    public function generaDni() {

    }

    public function generarDni() {
        $numero = rand(00000000, 99999999);
        $inicio = $numero % 23;
        $datos = [
            0 => "T", 1 => "R", 2 => "W", 3 => "A", 4 => "G", 5 => "M", 6 => "Y", 7 => "F", 8 => "P", 9 => "D", 10 => "X", 11 => "B",
            12 => "N", 13 => "J", 14 => "Z", 15 => "S", 16 => "Q", 17 => "V", 18 => "H", 19 => "L", 20 => "C", 21 => "K", 22 => "E"
        ];
        foreach ($datos as $key => $value) {
            if ($inicio === $key):
                var_dump($value);
                $letraDni = $value;
            endif;
        }
        $this->dni = $numero . $letraDni;
    }

}
