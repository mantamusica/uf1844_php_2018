<!DOCTYPE html>
<?php
spl_autoload_register(function($nombre) {
    include $nombre . '.php';
});
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $electrodomesticos = [];
        $elemento1 = new Electrodomestico();
        $elemento1->toString();
        $elemento1->precioFinal();
        $electrodomesticos[0] = $elemento1;
        echo '<br>------------------------<br>';        
        $elemento2 = new Electrodomestico(890, 'Verde', 'D', 130);
        $elemento2->toString();
        $electrodomesticos[1] = $elemento2;
        echo '<br>------------------------<br>';
        $elemento3 = new Lavadora(60);
        echo '<br>Probando metiendo solo carga';
        $elemento3->precioFinalLavadora();
        $electrodomesticos[2] = $elemento3;
        $elemento4 = new Lavadora(250,120);
        echo '<br>Probando metiendo precio base y peso';
        $elemento4->precioFinalLavadora();
        $electrodomesticos[3] = $elemento4;
        echo '<br>Probando con constructor vacío';
        $elemento5 = new Lavadora();
        $elemento5->precioFinalLavadora();
        $electrodomesticos[4] = $elemento5;
        echo '<br>------------------------<br>';
        $elemento6 = new Television();
        echo '<br>Probando constructor vacío';
        $elemento6->precioFinalTelevision();
        $electrodomesticos[5] = $elemento6;
        $elemento7 = new Television(50,TRUE);
        echo '<br>Probando metiendo resolución y sintonizador';
        $elemento7->precioFinalTelevision();
        $electrodomesticos[6] = $elemento7;
        echo '<br>Probando con constructor vacío';
        $elemento8 = new Television(60,200);
        $elemento8->precioFinalTelevision(); 
        $electrodomesticos[7] = $elemento8;
        echo '<br>------------------------<br>';
        $elemento9 = new Electrodomestico(890, 'Verde', 'D', 130);
        $elemento9->toString();
        $electrodomesticos[8] = $elemento9;    
        echo '<br>------------------------<br>';
        $elemento10 = new Lavadora(60);
        echo '<br>Probando metiendo solo carga';
        $elemento10->precioFinalLavadora();
        $electrodomesticos[9] = $elemento10;
        echo '<br>------------------------<br>';

        function preciosClases($electrodomesticos){
            
            for($i=0; $i < count($electrodomesticos);$i++){
                if(get_class($electrodomesticos[$i]) == 'Lavadora'){
                    $totalPrecioLavadora += $electrodomesticos[$i]->precioFinalLavadora();
                }else if(get_class($electrodomesticos[$i]) == 'Electrodomestico'){
                    $totalPrecioElectrodomestico += $electrodomesticos[$i]->precioFinal();
                }else{
                    $totalPrecioTelevision += $electrodomesticos[$i]->precioFinalTelevision();
                }
            }
        $precioFinal = $totalPrecioLavadora+ $totalPrecioElectrodomestico+$totalPrecioTelevision;    
        echo 'El precio total es de '.$precioFinal;
        }
        
       preciosClases($electrodomesticos);
        
                ?>
    </body>
</html>
