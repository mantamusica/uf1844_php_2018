<!DOCTYPE html>
<?php
spl_autoload_register(function($nombre) {
    include $nombre . '.php';
});
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $personaje = new Personaje('Manuel');
        $personaje->__construct0();
        $personaje->__construct1('David Clos', 23, 'perro');
        $personaje->__construct2('Ramón Abramo', 45, 'perro', 88, 1.63);
        $personaje->toString();
        ?>
    </body>
</html>
