<?php

class Electrodomestico {

    private $precioBase;
    private $color;
    private $consumoEnergetico;
    private $peso;

    const PRECIOBASE = 100;
    const COLOR = 'blanco';
    const CONSUMO = 'F';
    const PESO = 1;

    function __construct() {
        $params = func_get_args();
        $num_params = func_num_args();
        $funcion_constructor = '__construct' . $num_params;
        if (method_exists($this, $funcion_constructor)) {
            call_user_func_array(array($this, $funcion_constructor), $params);
        }
    }

    //ahora declaro una serie de métodos constructores que aceptan diversos números de parámetros
    function __construct0() {
        $this->precioBase=0;
        $this->color="Sin color";
        $this->consumoEnergetico = "Sin consumo energético";
        $this->peso = 0;  
    }

    function __construct2($precioBase, $color) {
        $this->precioBase=$precioBase;
        $this->color= $color;
        $this->consumoEnergetico = "Sin consumo energético";
        $this->peso = 0;
    }

    function __construct4($precioBase, $color, $consumoEnergetico, $peso) {
        $this->precioBase = $precioBase;
        $this->color = $color;
        $this->consumoEnergetico = $consumoEnergetico;
        $this->peso = $peso;
    }

    function setPrecioBase($precioBase) {
        $this->precioBase = $precioBase;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setConsumoEnergetico($consumoEnergetico) {
        $this->consumoEnergetico = $consumoEnergetico;
    }

    function setPeso($peso) {
        $this->peso = $peso;
    }

    function getPrecioBase() {
        return $this->precioBase;
    }

    function getColor() {
        return $this->color;
    }

    function getConsumoEnergetico() {
        return $this->consumoEnergetico;
    }

    function getPeso() {
        return $this->peso;
    }

    public function comprobarConsumoEnergetico() {
        $energia = ['A' => '100 €.', 'B' => '80 €.', 'C' => '80 €.', 'D' => '60 €.', 'E' => '40 €.', 'F' => '20 €.'];
        if (array_key_exists($this->consumoEnergetico, $energia)) {
            $this->consumoEnergetico = $this->consumoEnergetico;
        } else {
            $this->consumoEnergetico = CONSUMO;
        }
    }

    public function comprobarColor() {
        $colores = ['blanco', 'negro', 'rojo', 'azul', 'gris'];
        $string = srttolower($this->color);
        if (array_key_exists($string, $colores)) {
            $this->color = $this->color;
        } else {
            $this->color = COLOR;
        }
    }

    public function añadido_por_peso() {
        if ($this->peso > 0 && $this->peso <= 19) {
            $precioTamaño = 10;
        } else if ($this->peso > 19 && $this->peso <= 49) {
            $precioTamaño = 50;
        } else if ($this->peso > 49 && $this->peso <= 79) {
            $precioTamaño = 80;
        } else if ($this->peso > 79) {
            $precioTamaño = 100;
        } else {
            $precioTamaño = 0;
        }
        return $precioTamaño;
    }

    public function añadido_por_energia() {
        $letra = strtoupper($this->peso);
        if ($letra === 'A') {
            $precioEnergia = 100;
        } else if ($letra === 'B') {
            $precioEnergia = 80;
        } else if ($letra === 'C') {
            $precioEnergia = 60;
        } else if ($letra === 'D') {
            $precioEnergia = 50;
        } else if ($letra === 'E') {
            $precioEnergia = 30;
        } else if ($letra === 'F') {
            $precioEnergia = 10;
        } else {
            $precioEnergia = 0;
        }
        return $precioEnergia;
    }

    public function precioFinal() {
        $preciofinal = $this->precioBase + $this->añadido_por_peso() + $this->añadido_por_energia();
        return($preciofinal);
    }

    public function toString() {
        $string = '<br>Precio Base: ' . $this->precioBase . '<br>Color: ' . $this->color . '<br>Consumo Energético: ' . $this->consumoEnergetico . '<br>Peso: ' . $this->peso . '<br>';
        echo 'Información del Eletrodoméstico: <br>' . $string . '<br>Precio Final: ' . $this->precioFinal();
    }

}
