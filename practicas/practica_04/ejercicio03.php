<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $numeros = [12, 45, 6, 9, 89, 78, 12, 45, 45];

        function mediaAritmética($array) {
            $suma = array_sum($array);
            $media = $suma / count($array);
            return $media;
        }

        function modaArray($array) {
            $valores = array_count_values($array);
            arsort($valores);
            foreach ($valores as $key => $value) {
                $numeros[] = $key;
            }
            return $numeros[0];
        }

        function desviacionTipica($array) {
            $totalNumeros = count($array);
            foreach ($array as $value) {
                $suma [] = $value * $value;
            }
            foreach ($array as $value) {
                $media[] = $value;
            }
            $mediaArray = array_sum($media);
            $mediaArray = $mediaArray / count($media);
            $sumavalores = array_sum($suma);
            $calculo = sqrt(($sumavalores / $totalNumeros) - $mediaArray);
            return $calculo;
        }

        function mediana($array) {
            sort($array);
            $cantidad = count($array);
            $mediana = ($cantidad + 1) / 2;

            return $array[$mediana];
        }

        echo '<hr> Media Aritmética:';
        echo mediaAritmética($numeros);
        echo '<hr> Moda:';
        echo modaArray($numeros);
        echo '<hr> Desviación Típica:';
        echo desviacionTipica($numeros);
        echo '<hr> Mediana:';
        echo mediana($numeros);
        echo '<hr>';
        ?>
    </body>
</html>
