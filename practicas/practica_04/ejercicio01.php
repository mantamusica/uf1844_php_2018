<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        function ordenarCreciente($num1, $num2, $num3) {
            if ($num1 > $num2 && $num1 > $num3) {
                $mayor = $num1;
            } else if ($num2 > $num1 && $num2 > $num3) {
                $mayor = $num2;
            } else {
                $mayor = $num3;
            }

            if ($num1 < $num2 && $num1 < $num3) {
                $menor = $num1;
            } else if ($num2 < $num1 && $num2 < $num3) {
                $menor = $num2;
            } else {
                $menor = $num3;
            }

            $medio = ($num1 + $num2 + $num3) - $mayor - $menor;
            $salida = $menor . ', ' . $medio . ', ' . $mayor;
            return $salida;
        }

        echo ordenarCreciente(3500, 56, 99);
        ?>
    </body>
</html>
