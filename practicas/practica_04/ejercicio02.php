<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        function contarVocales($frase) {
            $contador = 0;
            $array_vocales = ["a", "e", "i", "o", "u"];
            $array_frase = str_split($frase);
            for ($i = 0; $i < count($array_vocales); $i++) {
                if (in_array($array_vocales[$i], $array_frase)) {
                    $contador++;
                }
            }

            return $contador;
        }

        echo contarVocales("Hola")
        ?>

    </body>
</html>
