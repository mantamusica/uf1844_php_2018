<?php

class Punto {
    
    private $coordenadaX;
    private $coordenadaY;
    
    function getCoordenadaX() {
        return $this->coordenadaX;
    }

    function getCoordenadaY() {
        return $this->coordenadaY;
    }

    function setCoordenadaX($coordenadaX) {
        $this->coordenadaX = $coordenadaX;
    }

    function setCoordenadaY($coordenadaY) {
        $this->coordenadaY = $coordenadaY;
    }
    
    function __construct($coordenadaX=0, $coordenadaY=0) {
        $this->coordenadaX = $coordenadaX;
        $this->coordenadaY = $coordenadaY;
    }



}
