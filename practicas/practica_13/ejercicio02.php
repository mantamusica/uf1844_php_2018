<!DOCTYPE html>
<?php
spl_autoload_register(function($nombre) {
    include $nombre . '.php';
});
?>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

    </head>
    <body>
        <?php
        $linea1=new Linea();
        $linea2=new Linea(new Punto(1,5.3),new Punto(4,8));
        $linea2->render();
        $linea2->mueveDerecha(5);
        $linea2->mueveAbajo(5);
        $linea2->render();
        ?>
    </body>
</html>
