<?php

class Linea {

    protected $_puntoA;
    protected $_puntoB;
    
    function __construct() {
        if(func_num_args()== 0){       
            $this->_puntoA = new Punto();
            $this->_puntoB = new Punto();
            
        }else{
            $argumentos= func_get_args();
            $this->__construct1($argumentos[0],$argumentos[1]);
            
        }
    }

    function __construct1($a, $b) {
        $this->_puntoA = $a;
        $this->_puntoB = $b;

    } 
    
    public function mueveDerecha($cantidad){

        $this->_puntoA->setCoordenadaX(+$cantidad);

    }
        public function mueveizquierda($cantidad){

        $this->_puntoA->setCoordenadaX(-$cantidad);

    }
    public function mueveArriba($cantidad){

        $this->_puntoB->setCoordenadaY(-$cantidad);

    }
        public function mueveAbajo($cantidad){

        $this->_puntoB->setCoordenadaY(+$cantidad);

    }    

    
    public function render(){
        $tercero = $this->_puntoA->getCoordenadaX().','.$this->_puntoA->getCoordenadaY();
        $cuarto = $this->_puntoB->getCoordenadaX().','.$this->_puntoB->getCoordenadaY();
        echo "[puntoA,puntoB].coordenadas:[(0.0,0.0),($tercero,$cuarto)]";
    }
    
    
    
    
}
