<?php

class Rectangulo {

    protected $posicionX;
    protected $posicionY;
    protected $altura;
    protected $base;

    function __construct1($posicionX, $posicionY, $base, $altura) {
        $this->posicionX = $posicionX;
        $this->posicionY = $posicionY;
        $this->base = $base;
        $this->altura = $altura;
    }

    function __construct2($base, $altura) {
        $this->posicionX = 0;
        $this->posicionY = 0;
        $this->base = $base;
        $this->altura = $altura;
    }

    public function calcularSuperficie($altura, $base) {
        $superficie = $base * $altura;
        return $superficie;
    }

    public function render($cantidad=0) {
        echo '<h1>Figura Rectángulo</h1>';
        echo '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" 2 xmlns:xlink="http://www.w3.org/1999/xlink" width="300" height="200" >';
        echo "<rect transform='translate($cantidad,0)' x='$this->posicionX' y='$this->posicionY' width='$this->base' height='$this->altura' fill='black' stroke='red'/> ";
        echo '</svg>';
        echo '<h2>La Superficie del rectángulo es ' . $this->calcularSuperficie($this->altura, $this->base) . '</h2>';
    }

}
