<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <p>Vamos a colocar en la página web los números del 1 al 10</p>

        <?php
        for ($i = 1; $i < 11; $i++):
            echo "<br>$i";
        endfor;
        ?>
        <hr>
        <p>Vamos a colocar los pesos de cada alumno</p>
        <?php
        $pesos = ["jaime" => 100, "manuel" => 91, "pepe" => 81];
        for ($i = 1; $i < 11; $i++):
            echo "<br>$i";
        endfor;
        ?>
        <hr>
        <table width="100%" border="1">
            <tr>
                <?php
                foreach ($pesos as $key => $value) {
                    echo '<td>';
                    echo $key;
                }
                echo '</td>';
                echo '</tr><tr>';
                foreach ($pesos as $key => $value) {
                    echo"<td>$value</td>";
                }
                ?>
            </tr>

        </tr>
    </table>
</body>
</html>
