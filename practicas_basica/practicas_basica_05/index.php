<!DOCTYPE html>
<?php
spl_autoload_register(function($nombre) {
    include $nombre . '.php';
});
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $newNombre = new Persona();
        $newNombre->inicializar('Jose María Cagigas Céspedes');
        $newNombre->imprimir();

        $newMenu = new Menu();
        $newMenu->cargarOpcion('Google', 'https://www.google.es/?gfe_rd=cr&dcr=0&ei=BWc3WtbXMoT_8Ae5hoGYBQ');
        $newMenu->cargarOpcion('Alpe', 'http://educacion.alpeformacion.es/index.php?');
        $newMenu->cargarOpcion('Microsoft', 'https://www.microsoft.com/es-es/');
        $newMenu->mostrar();

        $newCabecera = new Cabecera('Alpe Formación', 'center');
        $newCabecera->dibujar();

        $tabla1 = new Tabla(2, 3);
        $tabla1->cargar(1, 1, "Uno");
        $tabla1->cargar(1, 2, "Dos");
        $tabla1->cargar(1, 3, "Tres");
        $tabla1->cargar(2, 1, "Cuatro");
        $tabla1->cargar(2, 2, "Cinco");
        $tabla1->cargar(2, 3, "Seis");
        $tabla1->dibujar();
        ?>
    </body>
</html>
