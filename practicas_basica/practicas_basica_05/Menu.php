<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Menu
 *
 * @author chema
 */
class Menu {

    public $titulos=[];
    public $enlaces=[];
    

    function __construct() {

    }

    public function cargarOpcion($titulo,$enlace) {
        $this->titulos[]=$titulo;
        $this->enlaces[] = $enlace;
    }

    public function mostrar() {
        echo '<div><ul>';
        foreach ($this->titulos as $key => $value) {
            echo '<li type="circle"><a href=' . $this->enlaces[$key] . '>' . $value . '</a></li>';
        }
        echo '</ul></div>';
    }

}
