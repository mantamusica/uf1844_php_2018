<?php

/**
 * Description of Cabecera
 *
 * @author chema
 */
class Cabecera {

    public $titulo;
    public $situacion;

    function __construct($titulo, $situacion) {
        $this->titulo = $titulo;
        $this->situacion = $situacion;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getSituacion() {
        return $this->situacion;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setSituacion($situacion) {
        $this->situacion = $situacion;
    }

    public function dibujar() {
        echo '<br><h1 style="text-align:' . $this->situacion . ';">Alumnos de : ' . $this->titulo . '.</h1>';
    }

}
