<?php

/**
 * Description of Persona
 *
 * @author chema
 */
class Persona {

    public $nombre;

    function __construct() {

    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function inicializar($nombre) {
        $this->setNombre($nombre);
    }

    public function imprimir() {
        echo 'Página de : ' . $this->nombre . '.';
    }

}
