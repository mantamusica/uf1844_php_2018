<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script type="text/javascript">
            window.addEventListener("load", arranca);
            function arranca() {
                document.querySelector("#cero").addEventListener("click", ocultar);
                document.querySelector("#uno").addEventListener("click", ocultar);
                document.querySelector("#dos").addEventListener("click", ocultar);
            }
            function ocultar() {
                event.target.setAttribute("display", 'none');
            }

        </script>
    </head>
    <body>
        <?php
        /*
         * Function que nos da un color aleatorio rgb
         */

        function color() {
            $color = "rgb(" . rand(0, 255) . "," . rand(0, 255) . "," . rand(0, 255) . ")";
            return $color;
        }

        /*
         * Function para dibujar círculos en la que introducimos como argumentos
         * Los ejes x e y como arg1 y arg2, el radio del circulo como arg3 y
         * numero que le damos al circulo para trabajar con la función de ocultar
         */

        function dibujarCirculo($arg1, $arg2, $arg3, $numeroCirculo) {
            ?>
            <circle id="<?= $numeroCirculo ?>" cx="<?= $arg1 ?>" cy="<?= $arg2 ?>" r="<?= $arg3 ?>" fill="<?= color() ?>"/>
            <?php
        }
        ?>
        <p>Color : <?= color() ?></p>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="1200" height ="1200" style="display: block; margin: 0px auto;">
        <?php
        dibujarCirculo(100, 50, 60, 'cero');
        dibujarCirculo(30, 30, 30, 'uno');
        dibujarCirculo(212, 180, 100, 'dos');
        ?>
        </svg>
    </body>
</html>
