<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script type="text/javascript">
            window.addEventListener("load", arranca);
            function arranca() {
                document.querySelector("line").addEventListener("click", salta);
            }
            function salta() {
                var valor;
                valor = Math.random() * (100 - 1) + 1;
                event.target.setAttribute("x2", valor);
            }
        </script>
    </head>
    <body>
        <?php
        $longitud = rand(10, 1000);

        print "             <p> Longitud: $longitud.</p>";

        echo "</hr>";
        ?>

        <svg width = "1000" height = "100" >
        <line x1 ="1" y1="5" x2="<?= $longitud ?>" y2="5" stroke="red" stroke-width="10"/>
        </svg>
    </body>
</html>
