<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /*
         * Function que nos da un color aleatorio rgb
         */

        function color() {
            $color = "rgb(" . rand(0, 255) . "," . rand(0, 255) . "," . rand(0, 255) . ")";
            return $color;
        }

        $color = color();

        function dibujarCirculo($arg1, $arg2) {
            ?>
            <circle cx="<?= $arg1 ?>" cy="<?= $arg2 ?>" r="50" fill="<?= $color ?>"/>
            <?php
        }
        ?>
        <p>Color : <?= $color ?></p>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="100" height ="100" style="display: block; margin: 0px auto;">
        <?php
        dibujarCirculo(100, 50);
        dibujarCirculo(30, 30);
        dibujarCirculo(212, 180);
        ?>
        </svg>
    </body>
</html>
