<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Jugada
 *
 * @author chema
 */
class Jugada {
    public $dados=[];
    public $total;
    
    public function __construct() {
        $this->total=0;
        $this->dados[]=new Dados($this->tirada());
        $this->dados[]=new Dados($this->tirada());
        $this->total=$this->dados[0]->getValor() + $this->dados[1]->getValor();
    }
    
    protected function tirada(){
        return rand(1, 6);
    }
    
    public function render(){
        echo "<div>";
        echo $this->dados[0]->dibujar();
        echo $this->dados[1]->dibujar();
        echo "</div>";
        echo "<div>";
        echo 'Total: '.$this->total;
        echo "</div>";
    }
    

}
