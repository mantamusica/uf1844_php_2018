<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $contador = 0;
        $sumaTiradas = [];

        for ($c = 0; $c < 10; $c++) {
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
            $sumaTiradas[] = $d1 + $d2;
            ?>
            <div>
                <div class="dados">
                    <img src="imgs/<?php $d1 ?>.svg" alt="dado1"/>
                    <img src="imgs/<?php $d2 ?>.svg" alt="dado2"/>
                </div>
            </div>
            <div class="total">Total: <span> <?= $sumaTiradas[$contador++] ?></span></div>
            <?php
        }
        var_dump($sumaTiradas);
        ?>
        <div class="mayor">La tirada mayor ha sido de <?= max($sumaTiradas) ?></div>
    </body>
</html>
