<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            img {width:50px; height:50px;} span {color: red; font-size:20px;}
        </style>
    </head>
    <body>
        <?php
        $tiradas = [];
        $sumaTiradas = [];

        function tirada(&$d1, &$d2) {
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
        }

        for ($c = 0; $c < 10; $c++) {
            tirada($tiradas[$c]['dado1'], $tiradas[$c]['dado2']);
            $sumaTiradas[] = $tiradas[$c]['dado1'] + $tiradas[$c]['dado2'];
        }

        foreach ($tiradas as $key => $value) {
            ?>
            <div>
                <div class="dados">
                    <img src="imgs/<?php echo $value['dado1'] ?>.svg" alt="dado1"/>
                    <img src="imgs/<?php echo $value['dado2'] ?>.svg" alt="dado2"/>
                </div>
                <div class="total">Total: <span> <?= $sumaTiradas[$key] ?></span></div>
            </div>
            <?php
        }
        ?>
    </body>
</html>
