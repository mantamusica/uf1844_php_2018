<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Juego
 *
 * @author chema
 */
class Juego {
    protected $jugadas=[];
    public $mayor;
    protected $numeroTiradas;
    public $totales=[];
    
    public function __construct($n=10) {
        $this->numeroTiradas=$n;
        $this->crearJugadas();
        $this->maximo();
    }
    
    private function crearJugadas(){
        for($c=0;$c<$this->numeroTiradas;$c++){
            $this->jugadas[$c]=new Jugada();
            $this->totales[$c]=$this->jugadas[$c]->total;
        }
    }
    
    private function maximo(){
        $this->mayor=max($this->totales);
    }
    
    public function render(){
        for($c=0;$c<$this->numeroTiradas;$c++){
            $this->jugadas[$c]->render();
        }
        echo "El valor mayor es:" .$this->mayor;
    }
            

    
    
            
}
