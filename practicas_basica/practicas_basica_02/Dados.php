<?php


class Dados {
    protected $valor;
    protected $ruta;
    
    function __construct($valor) {
        $this->valor = $valor;
        $this->ruta="imgs/$valor.svg";
    }

    public function dibujar(){
        return '<img src="' . $this->ruta . '">';
    }
    
    function getValor() {
        return $this->valor;
    }


}
