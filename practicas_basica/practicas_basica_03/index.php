<!DOCTYPE html>
<?php
session_start();
//session_destroy();
if (!isset($_SESSION['partida'])) {
    $_SESSION['partida'] = [];
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            .dados{padding: 10px;}
            .cuadro{border: thin solid black; width:100px;position: relative; height: 100px; font-size: 90px; float: left; text-align: center}
            .dados img {width: 50px; height: 50px}
            .tablero{padding: 10px; position: absolute;}
            .tablero img{width: 90px; height: 90px; z-index: 1000; position: absolute;top: 5px; left: 5px;}
        </style>
    </head>
    <body>
        <h1>Juegos Raro</h1>
        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
            <input type="submit" value="Limpiar" name="limpiar">&nbsp;<input type="submit" name ="tirar" value="Tirar">
        </form>
        <?php
        if (isset($_POST['tirar'])) {
            $_SESSION['partida'][] = rand(1, 6);
            $saltos = '<br><br><br><br><br><br>';
            foreach ($_SESSION['partida'] as $key => $value) {
                echo '<h2>Jugada número ' . $key . ' con el número ' . $value . '</h2>';
                echo '<div class="dados">';
                echo '<img src="img/' . $value . '.svg" alt="dado"/>';
                echo '</div>';
                echo '<div class="tablero">';
                for ($c = 1; $c <= 6; $c++) {
                    if ($value != $c) {
                        echo '<div class="cuadro">' . $c . '</div> ';
                    } else {
                        echo '<div class="cuadro"><img src="img/punto.png" alt="punto"/>' . $value . '</div> ';
                    }
                }
                echo '</div>';
                echo $saltos;
            }
        }


        if (isset($_POST['limpiar'])) {
            unset($_SESSION['partida']);
            header("Refresh:0; url=index.php");
        }
        ?>
    </body>
</html>
