<?php

class Coche {

    var $color;
    var $numero_puertas;
    var $marca;
    var $gasolina = 0;

    function llenarTanque($gasolina_nueva) {
        $this->gasolina = $this->gasolina + $gasolina_nueva;
    }

    function acelerar() {
        if ($this->gasolina > 0) {
            $this->gasolina = $this->gasolina - 1;
            return "Gasolina restante: " . $this->gasolina;
        }
    }

    function getColor() {
        return $this->color;
    }

    function getNumero_puertas() {
        return $this->numero_puertas;
    }

    function getMarca() {
        return $this->marca;
    }

    function getGasolina() {
        return $this->gasolina;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setNumero_puertas($numero_puertas) {
        $this->numero_puertas = $numero_puertas;
    }

    function setMarca($marca) {
        $this->marca = $marca;
    }

    function setGasolina($gasolina) {
        $this->gasolina = $gasolina;
    }

    function __construct($color, $numero_puertas, $marca, $gasolina) {
        $this->color = $color;
        $this->numero_puertas = $numero_puertas;
        $this->marca = $marca;
        $this->gasolina = $gasolina;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

