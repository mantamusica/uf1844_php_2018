<?php

class Vehiculo {

    public $matricula;
    private $color;
    protected $encendido = false;
    public static $ruedas = 5;

    function __construct($matricula, $color, $encendido) {
        $this->matricula = $matricula;
        $this->color = $color;
        $this->encendido = $encendido;
    }

    function __construct1($matricula, $color) {
        $this->matricula = $matricula;
        $this->color = $color;
    }

    function __construct2($matricula) {
        $this->matricula = $matricula;
    }

    public function encender() {
        $this->encendido = true;
        echo "Vehiculo encendido <br>";
    }

    public function apagar() {
        $this->encendido = false;
        echo "Vehiculo apagado <br>";
    }

    static function mensaje() {
        echo 'este es mi coche <br>';
    }

    static function ruedas() {
        echo Vehiculo::$ruedas;
    }

    function getMatricula() {
        return $this->matricula;
    }

    function getColor() {
        return $this->color;
    }

    function getEncendido() {
        return $this->encendido;
    }

    function setMatricula($matricula) {
        $this->matricula = $matricula;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setEncendido($encendido) {
        $this->encendido = $encendido;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>