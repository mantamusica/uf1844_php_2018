<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        class Usuario {

            var $nombre = "defecto";
            private $edad;
            protected $telefono;

            function getNombre() {
                return $this->nombre;
            }

            function getEdad() {
                return $this->edad;
            }

            function getTelefono() {
                return $this->telefono;
            }

            function setNombre($nombre) {
                $this->nombre = $nombre;
            }

            function setEdad($edad) {
                $this->edad = $edad;
            }

            function setTelefono($telefono) {
                $this->telefono = $telefono;
            }

        }

        $persona = new Usuario();
        echo $persona->nombre;
        $persona->setEdad(51);
        $persona->setTelefono(942622390);
        var_dump($persona);
        $persona->nombre = "silvia";
        $persona->setEdad(42);
        $persona->setTelefono(942622391);
        var_dump($persona);
        ?>
    </body>
</html>
