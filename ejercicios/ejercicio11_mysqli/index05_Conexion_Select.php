<?php

/**
 * Description of Conexion
 *
 * @author chema
 */
class Conexion {

    public $equipo;
    public $usuario;
    public $password;
    public $conexion;
    public $bbdd;
    public $variable_consulta;
    public $tabla;
    public $campos;
    public $consulta;

    function __construct($equipo = "127.0.0.1", $usuario = "root", $password = "", $bbdd = "") {
        $this->equipo = $equipo;
        $this->usuario = $usuario;
        $this->password = $password;
        $this->bbdd = $bbdd;
        $this->conectar();
    }

    public function crearConsulta() {
        $this->consulta = "SELECT $this->campos from $this->tabla;";
    }
    
    public function mostrarBBDD(){
        $this->consulta = "show databases";
    }

    public function setTabla($tabla) {
        $this->tabla = $tabla;
        $this->crearConsulta();
    }

    public function setCampos($campos) {
        $this->campos = $campos;
        $this->crearConsulta();
    }

    public function conectar() {
        if (empty($this->bbdd)) {
            $this->conexion = new mysqli($this->equipo, $this->usuario, $this->password)
                    or die("no se ha podido establecer conexion con el servidor" . $this->conexion->error);
        } else {
            $this->conexion = new mysqli($this->equipo, $this->usuario, $this->password, $this->bbdd)
                    or die("no se ha podido establecer conexion con el servidor" . $this->conexion->error);
        }
    }

    public function seleccionar() {
        $this->conexion->select_db($this->bbdd)
                or die("no pudo seleccionarse la bd." . $this->conexion->error);
        $this->castellano();
    }

    public function listar_registros() {
        $titulo = 1;

        echo '<table width="100%" border="1">';
        while ($row = ($this->variable_consulta->fetch_assoc())) {
            if ($titulo) {
                echo '<tr>';
                foreach ($row as $campo => $dato) {
                    echo '<td>';
                    echo $campo;
                    echo '</td>';
                }
                echo '</tr>';
                $titulo = 0;
            }
            echo '<tr>';
            foreach ($row as $dato) {
                echo '<td>';
                echo $dato;
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
        $this->variable_consulta->data_seek(0);
    }

    public function listar_registros_select($campoEnviar, $camposMostrar, $nombre) {
        echo '<select name"' . $nombre . '">';
        while ($row = $this->variable_consulta->fetch_array()) {
            echo '<option value="' . $row[$campoEnviar] . '">';
            echo $this->unirResultados($row, $camposMostrar);
            echo '</option>';
        }
        echo '</select>';
        $this->variable_consulta->data_seek(0);
    }

    public function listar_registros_lista($campoEnviar, $camposMostrar, $nombre, $tamaño = '5') {
        echo '<select name"' . $nombre . '" size="' . $tamaño . '">';
        while ($row = $this->variable_consulta->fetch_array()) {
            echo '<option value="' . $row[$campoEnviar] . '">';
            echo $this->unirResultados($row, $camposMostrar);
            echo '</option>';
        }
        echo '</select>';
        $this->variable_consulta->data_seek(0);
    }

    public function unirResultados($row, $camposMostrar) {
        $salida = "";
        if (!is_array($camposMostrar)) {
            $camposMostrar = explode(",", $camposMostrar);
        }
        foreach ($camposMostrar as $v) {
            $salida .= " " . $row[$v];
        }
        return $salida;
    }

    public function liberarResultados() {
        $this->variable_consulta->close();
    }

    public function castellano() {
        $this->conexion->query("SET NAMES 'UTF-8'");
    }

    public function consulta() {
        echo $this->consulta;
        $this->variable_consulta = $this->conexion->query($this->consulta)
                or die("No pudo realizar la consulta" . $this->conexion->error);
    }

    public function __destruct() {
        $this->conexion->close();
    }

}

$objeto = new Conexion();
echo '<br> ----------------------------------------<br>';
$objeto->bbdd = "videoteca";
$objeto->seleccionar();
$objeto->setCampos("*");
$objeto->setTabla("pelicula");
$objeto->consulta();
$objeto->listar_registros();
echo '<br> ----------------------------------------<br>';
$objeto->setTabla("actor");
$objeto->consulta();
$objeto->listar_registros();
$objeto->listar_registros_select("id", "id,nombre,apellidos", "idActor");
$objeto->listar_registros_lista("id", "id,nombre,apellidos", "idActor");
$objeto->liberarResultados();
echo '<br> ----------------------------------------<br>';
$objeto->setTabla("director");
$objeto->consulta();
$objeto->listar_registros();
$objeto->listar_registros_select("id", "id,nombre,apellidos", "idDirector");
$objeto->listar_registros_lista("id", "id,nombre,apellidos", "idDirector");
$objeto->liberarResultados();
echo '<br> ----------------------------------------<br>';


