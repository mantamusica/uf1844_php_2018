<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            table{
                width: 800px; margin: 50px auto;
            }
        </style>
    </head>
    <body>
        <?php
        //Variables de conexion al servidor de mysql
        $equipo = "127.0.0.1";
        $usuario = "root";
        $password = "";
        $conexion;
        $bbdd = "videoteca";
        $variable_consulta;
        $tabla = "pelicula";
        $campos = "*";
        $consulta = "SELECT $campos FROM $tabla";

        function conectarGlobales() {
            global $equipo;
            global $usuario;
            global $password;
            global $conexion;
            global $bbdd;

            $conexion = mysqli_connect($equipo, $usuario, $password, $bbdd)
                    or die("No se ha encontrado conexión.");
        }

        function seleccionar() {
            global $bbdd;
            global $conexion;
            mysqli_select_db($bbdd)
                    or die('No pudo seleccionar la BD.' . $mysqli_error($conexion));
            castellano();
        }

        function castellano() {
            global $conexion;
            //Coloco el codigo de caracteres para que salga castellano
            mysqli_query($conexion, "SET NAMES 'utf-8'");
        }

        function listar_registros() {
            global $variable_consulta;
            global $consulta;
            $titulo = 1;

            echo '<table width="100%" border="1">';
            while ($row = mysqli_fetch_assoc($variable_consulta)) {
                if ($titulo) {
                    echo '<tr>';
                    foreach ($row as $campo => $dato) {
                        echo '<td>';
                        echo $campo;
                        echo '</td>';
                    }
                    echo '</tr>';
                    $titulo = 0;
                }
                echo '<tr>';
                foreach ($row as $dato) {
                    echo '<td>';
                    echo $dato;
                    echo '</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
            mysqli_data_seek($variable_consulta, 0);
        }

        function consulta() {
            global $variable_consulta;
            global $consulta;
            global $conexion;

            $variable_consulta = mysqli_query($conexion, $consulta)
                    or die('no pudo realizar la consulta.' . mysqli_error($conexion));
        }

        conectarGlobales();
        //seleccionar();
        castellano();
        consulta();
        listar_registros();
        ?>
    </body>
</html>
