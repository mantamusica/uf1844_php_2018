<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            table{
                width: 800px; margin: 50px auto;
            }
        </style>
    </head>
    <body>
        <?php
        //Variables de conexion al servidor de mysql
        $equipo = "127.0.0.1";
        $usuario = "root";
        $password = "";
        $conexion;
        $bbdd = "videoteca";
        $variable_consulta;
        $tabla = "pelicula";
        $campos = "*";
        $consulta = "SELECT $campos FROM $tabla";

        function conectarGlobales() {
            global $equipo;
            global $usuario;
            global $password;
            global $conexion;
            global $bbdd;

            $conexion = new mysqli($equipo, $usuario, $password, $bbdd)
                    or die("No se ha encontrado conexión.");
        }

        function seleccionar() {
            global $bbdd;
            global $conexion;
            $conexion->select_db($bbdd)
                    or die('No pudo seleccionar la BD.' . $mysqli_error($conexion));
            castellano();
        }

        function castellano() {
            global $conexion;
            //Coloco el codigo de caracteres para que salga castellano
            $conexion->query("SET NAMES 'UTF-8'");
        }

        function listar_registros() {
            global $variable_consulta;
            global $consulta;
            $titulo = 1;

            echo '<table width="100%" border="1">';
            while ($row = ($variable_consulta->fetch_assoc())) {
                if ($titulo) {
                    echo '<tr>';
                    foreach ($row as $campo => $dato) {
                        echo '<td>';
                        echo $campo;
                        echo '</td>';
                    }
                    echo '</tr>';
                    $titulo = 0;
                }
                echo '<tr>';
                foreach ($row as $dato) {
                    echo '<td>';
                    echo $dato;
                    echo '</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
            $variable_consulta->data_seek(0);
        }

        function consulta() {
            global $variable_consulta;
            global $consulta;
            global $conexion;

            $variable_consulta = $conexion->query($consulta)
                    or die('no pudo realizar la consulta.' . $conexion->error);
        }

        conectarGlobales();
        //seleccionar();
        castellano();
        consulta();
        listar_registros();
        ?>
    </body>
</html>
