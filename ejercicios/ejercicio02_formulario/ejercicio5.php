<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if ($mal) {
            ?>
            <form name = "f">
                <select multiple name="ciudad[]">
                    <option value="SS">Santander</option>
                    <option value="PA">Palencia</option>
                    <option value="PO">Potes</option>
                </select>
                <input type="submit" value="Enviar" name="boton" />
            </form>
            <?php
        } else {
            $ciudades = array(
                "SS" => "santander",
                "PA" => "Palencia",
                "PO" => "Potes"
            );
            echo "los elementos seleccionados son: ";
            foreach ($_GET['ciudad'] as $value) {
                echo "<br>$value-$ciudades[$value]"; // muestra codigos
            }
        }
        ?>        

    </body>
</html>


