<?php

namespace Clase;

/**
 * Description of Aplicacion
 *
 * @author ramon
 */
class Aplicacion {
    protected $tiradas=[];
    protected $mayor;
    private $numero;

    public function __construct($n=10) {
        $this->numero=$n;
        $this->mayor=0;
        $this->realizarTiradas();
    }

    private function realizarTiradas(){
        for($c=0;$c<$this->numero;$c++){
            $this->tiradas[$c]=new Tirada();
            if($this->tiradas[$c]->puntuacionTotal>$this->mayor){
                $this->mayor=$this->tiradas[$c]->puntuacionTotal;
            }
        }
    }
    
    public function render(){
        echo "<div>";
        for($c=0;$c<$this->numero;$c++){
            echo $this->tiradas[$c]->render();
        }
        echo "El valor mayor es: " . $this->mayor;
        echo "</div>";
    }
            
            
            
            
            
            
            
            
            
            
            
            
}
