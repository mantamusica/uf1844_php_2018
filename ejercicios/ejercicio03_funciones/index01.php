<!DOCTYPE html>


<?php
include('funciones.php');
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        Ejercicio1
        <?php
        $salida = ejercicio1(1, 5, 10);
        var_dump($salida);
        ?>
        Ejercicio3
        <?php
        $arrayColores = generaColores(5);
        var_dump($arrayColores);
        ?>
        Ejercicio4
        <?php
        $arrayColores = generaColores2(5, false);
        var_dump($arrayColores);
        ?>
        Ejercicio5
        <?php
        $repetidos = elementosRepetidos($salida, false);
        var_dump($repetidos);
        ?>
        <?php
        $directorio = opendir("."); //ruta actual
        while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
            if (is_dir($archivo)) {//verificamos si es o no un directorio
                echo "[" . $archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
            } else {
                echo $archivo . "<br />";
            }
        }
        ?>

        Ejercicio6
        <?php
        $directorios = leerDirectorio(".");
        var_dump($directorios);
        ?>
    </body>
</html>
