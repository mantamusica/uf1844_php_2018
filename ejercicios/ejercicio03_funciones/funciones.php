<?php

/**
 *
 * @param type $min, valor mínimo a introducir
 * @param type $max, valor máximo a introducir
 * @param array $valores, tamaño del array
 */
function ejercicio1($min, $max, $valores) {
    for ($i = 0; $i < $valores; $i++) {
        $salida[] = rand($min, $max);
    }
    return $salida;
}

/**
 *
 * @param type $min
 * @param type $max
 * @param type $valores
 * @param type $arraySalida
 */
function ejercicio2($min, $max, $valores, $arraySalida) {
    for ($i = 0; $i < $valores; $i++) {
        $salida[] = rand($min, $max);
    }
}

/**
 *
 * @param type $numero
 * @return array de colores generados
 */
function generaColores($numero) {
    $arrayColores = [];
    for ($n = 0; $n < $numero; $n++) {
        $arrayColores[$n] = "#";
        for ($c = 0; $c < 7; $c++) {
            $arrayColores[$n] .= dechex(mt_rand(0, 15));
        }
    }
    return $arrayColores;
}

/**
 * Función que genera colores
 * @param int $numero, número de colores a generar
 * @param bool $almohadilla=true con valor true nos indica que coloquemos la almohadilla
 * @return array los colores solicitados en un array de cadenas
 */
function generaColores2($numero, $almohadilla = TRUE) {
    $colores = [];
    for ($n = 0; $n < $numero; $n++) {
        $limite = 6;
        $colores[$n] = "";
        if ($almohadilla) {
            $colores[$n] = "#";
            $limite = 7;
        }
        for ($c = 0; $c < $limite; $c++) {
            $colores[$n] .= dechex(mt_rand(0, 15));
        }
    }
    return $colores;
}

/**
 *
 * @param array que introducimos que va a mirar a ver si tenemos mienbros repetidos
 * @param bool $devolverTodos
 * @return int
 */
function elementosRepetidos($array, $devolverTodos = false) {
    $repetidos = [];

    foreach ((array) $array as $value) {
        $inArray = false;

        foreach ($repetidos as $key => $value) {
            if ($key['value'] === $value) {
                $inArray = true;
                ++$repetidos[$i]['count'];
            }
        }
        if (false === $inArray) {
            $i = count($repetidos);
            $repetidos[$i] = [];
            $repetidos[$i]['value'] = $value;
            $repetidos[$i]['count'] = 1;
        }
    }

    if (!$devolverTodos) {
        foreach ($value as $indice => $valor) {
            if ($indice['count'] === 1) {
                unset($repetidos[$i]);
            }
        }
    }

    sort($repetidos);

    return $repetidos;
}

/**
 * Función que te permite generar un array con el contenido de un directorio.
 * Salida ordenada
 * @param type $handle cadena de caracteres con la ruta del directorio a listar
 * @return type string, array con el nombre de los ficheros y directorios
 */
function leerDirectorio($handle = " . ") {
    $handle = opendir($handle);
    while (false !== ($archivo = readdir($handle))) {
        $archivos[] = strtolower($archivo);
    }
    closedir($handle);
    sort($archivos);

    return $archivos;
}
?>