<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cadena
 *
 * @author chema
 */
class Cadena {

    public static function masyusculas($cadena) {
        return mb_strtoupper($cadena, "UTF-8");
    }

}

var_dump(Cadena::masyusculas("Ejemplo de metodo"));

//no es necesario
$v = new Cadena();
var_dump($v->masyusculas("Metodo"));
?>


