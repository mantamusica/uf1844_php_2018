<?php

class Animal {

    var $name;

    function set_name($text) {
        $this->name = $text;
    }

    function get_name() {
        return $this->name;
    }

}

class Leon extends Animal {

    var $name;

    function roar() {
        echo "=>", $this->name, "está rugiendo!<br>";
    }

    function set_name($text) {
        Animal::set_name($text);
    }

}

echo "creando su nuevo león ....<br>";
$leon = new Leon;
$leon->set_name("superman ");
$leon->roar();
