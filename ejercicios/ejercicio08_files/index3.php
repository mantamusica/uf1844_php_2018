<?php

class Animal {

    public $name;

    public function set_name($text) {
        $this->name = $text;
    }

    public function get_name() {
        return $this->name;
    }

}

class Leon extends Animal {

    public $name;

    public function roar() {
        echo "=>", $this->name, "está rugiendo!<br>";
    }

    public function set_name($text) {
        parent::set_name($text);
    }

}

echo "creando su nuevo león ....<br>";
$leon = new Leon;
$leon->set_name("superman ");
$leon->roar();
