<?php

class Animal {

    public $name;

    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }

}

class Leon extends Animal {

    public $name;

    public function roar() {
        echo "=>" . $this->name . " está rugiendo!<br>";
    }

    public function setName($text) {
        parent::setName($text);
    }

}

echo "Creando su nuevo león .....<br>";
$leon = new Leon;
$leon->setName("Superman");
$leon->roar();
?>
