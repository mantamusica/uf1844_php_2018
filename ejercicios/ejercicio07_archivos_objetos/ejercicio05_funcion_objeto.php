<?php

class Cadena {

    public static function mayusculas($cadena) {
        return mb_strtoupper($cadena, 'UTF-8');
    }

}

var_dump(Cadena::mayusculas('Ejemplo de esta clase.'));

//no es necesario
$v = new Cadena();
var_dump($v->mayusculas('prueba segunda'));
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

