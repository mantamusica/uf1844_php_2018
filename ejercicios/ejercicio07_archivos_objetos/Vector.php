<?php

class Vector {

    private $valor;
    private $fragmentos;
    private $longitud;

    function __construct($valor = [1, 2, 3, 4, 5, 6, 7, 8, 9]) {
        $this->valor = $valor;
        $this->setLongitud();
    }

    function getValor() {
        return $this->valor;
    }

    function getFragmentos() {
        return $this->fragmentos;
    }

    function getLongitud() {
        return $this->longitud;
    }

    function setValor($valor) {
        $this->valor = $valor;
    }

    function setFragmentos($fragmentos) {
        $this->fragmentos = $fragmentos;
    }

    function setLongitud() {
        $this->longitud = count($this->valor);
    }

    public function rDerecha($posiciones) {
        for ($index = 0; $index < $posiciones; $index++) {
            $a[$index] = array_pop($this->valor);
        }
        $this->valor = array_merge($a, $this->valor);
    }

    public function rDerecha1($posiciones) {
        $a = array_slice($this->valor, 0, $this->longitud - $posiciones);
        $b = array_slice($this->valor, $this->longitud - $posiciones);
        $this->valor = array_merge($b, $a);
    }

    public function trocear($elementos) {
        $this->fragmentos = array_chunk($this->valor, $elementos);
    }

    public static function cadenaArray($string) {
        $strlen = mb_strlen($string);
        while ($strlen) {
            $array[] = mb_substr($string, 0, 1, "UTF-8");
            $string = mb_substr($string, 1, $strlen, "UTF-8");
            $strlen = mb_strlen($string);
        }
        return $array;
    }

}
