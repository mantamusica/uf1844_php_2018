<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $cadena = "ES españa la más cañi?";

        function cadenaArray($string) {
            $strlen = strlen($string);
            while ($strlen) {
                $array[] = substr($string, 0, 1);
                $string = substr($string, 1, $strlen);
                $strlen = strlen($string);
            }
            return $array;
        }

        var_dump(cadenaArray($cadena));

        function cadenaArray2($string) {
            $strlen = strlen($string);
            for ($i = 0; $i < strlen($string); $i++) {
                $array[$i] = $string[$i];
            }
            return $array;
        }

        var_dump(cadenaArray2($cadena));

        function cadenaArray3($string) {
            $strlen = mb_strlen($string);
            while ($strlen) {
                $array[] = mb_substr($string, 0, 1, "UTF-8");
                $string = mb_substr($string, 1, $strlen, "UTF-8");
                $strlen = mb_strlen($string);
            }
            return $array;
        }

        var_dump(cadenaArray3($cadena));
        ?>
    </body>
</html>
