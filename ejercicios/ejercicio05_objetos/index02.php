<!DOCTYPE html>
<?php

function __autoload($nombre_clase) {
    include $nombre_clase . '.php';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $padre = new Persona("ramon", "abrama", 35);
        $hijo = $padre;
        $hija = clone $padre;
        $hijo->setEdad(100);
        $hija->setEdad(50);
        var_dump($hijo);
        var_dump($padre);
        var_dump($hija);
        ?>
    </body>
</html>
