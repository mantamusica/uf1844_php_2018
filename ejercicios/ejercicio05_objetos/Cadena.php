<?php

class Cadena {

    private $valor;
    private $longitud;
    private $vocales;

    function __construct($valor) {
        $this->valor = $valor;
    }

    function getValor($minusculas = false) {
        if ($minusculas) {
            return strtolower($this->valor);
        } else {
            return $this->valor;
        }
    }

    function getLongitud() {
        $this->getLongitud();
        return $this->longitud;
    }

    function getVocales() {
        $this->numeroVocales();
        return $this->vocales;
    }

    function setValor($valor) {
        $this->valor = $valor;
    }

    function setLongitud($longitud) {
        $this->longitud = $longitud;
    }

    function setVocales($vocales) {
        $this->vocales = $vocales;
    }

    public function calcularLongitud() {
        $this->setLongitud(strlen($this->valor));
    }

    public function repeticionVocal($vocal) {
        $longitud = 0;
        $longitud += substr_count($this->getValor(1), $vocal);
    }

    public function numeroVocales() {
        $vocales = ['a', 'e', 'i', 'o', 'u'];
        $longitud = 0;
        foreach ($vocales as $value) {
            $longitud += substr_count($this->getValor(1), $valor);
        }
        $this->setVocales($longitud);
    }

}
