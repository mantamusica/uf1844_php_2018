<?php

class Camion extends Vehiculo {

    private $carga;

    public function cargar($cantidad) {
        $this->carga = $cantidad;
        echo'Se ha cargado la cantidad de :' . $cantidad . '<br>';
    }

    public function verificar_encendido() {
        if ($this->encendido == true) {
            echo 'Camion encendido <br>';
        } else {
            echo 'Camion apagado';
        }
    }

}
