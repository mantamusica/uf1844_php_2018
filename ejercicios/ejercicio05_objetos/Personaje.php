<?php

class Personaje {

    private $nombre;

    function __construct($nombre) {
        $this->nombre = $nombre;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this;
    }

    public function dormir() {

    }

    public function hablar() {

    }

}
