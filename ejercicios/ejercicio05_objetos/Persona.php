<?php

class Persona {

    private $nombre;
    private $apellidos;
    private $nombreCompleto;
    protected $edad;

    function __construct($nombre, $apellidos, $edad) {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->setNombreCompleto();
        $this->edad = $edad;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellidos() {
        return $this->apellidos;
    }

    function getNombreCompleto() {
        return $this->nombreCompleto;
    }

    function getEdad() {
        return $this->edad;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this;
    }

    function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
        return $this;
    }

    function setNombreCompleto($nombreCompleto) {
        $this->nombreCompleto = $this->getNombre() . " " . $this->getApellidos();
        return $this;
    }

    function setEdad($edad) {
        $this->edad = $edad;
        return $this;
    }

}
