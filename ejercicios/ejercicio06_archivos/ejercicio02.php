<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if (!$_POST) {
            ?>
            <form method="POST" enctype="multipart/form/data">
                <input type="file" name="archivo">
                <input type="submit" value="enviar" name="send">
                <input type="reset" value="borrar" name="erase">
            </form>
            <?php
        } else {
            if ($_FILES["archivo"]['type'] == 'image/jpeg') {
                move_uploaded_file($_FILES["archivo"]['tmp_name'], ".\\" . $_FILES["archivo"]['name']);
                echo 'El archivo se ha subido correctamente al servidor.';
            } else {
                echo "Tipo de archivo incorrecto.";
            }
        }
        ?>
    </body>
</html>
