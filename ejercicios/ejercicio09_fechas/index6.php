<?php

function mostrar() {
    static $c = 0;
    foreach (func_get_args() as $valor) {
        echo "<br> $c-$valor<br>";
        $c++;
    }
}

/*
 * Vamos a analizar la funcion strtotime();
 */

$fecha = "2000/12/25";

mostrar(date("d/m/y", strtotime($fecha)));
mostrar(date("d/m/y", strtotime('now')));
mostrar(date("d/m/y", strtotime('+1 day')));
mostrar(date("d/m/y", strtotime('+1 day', strtotime($fecha))));
mostrar(date("d/m/y", strtotime('previous monday')));
