<?php

function mostrar() {
    static $c = 0;
    foreach (func_get_args() as $valor) {
        echo "<br> $c-$valor<br>";
        $c++;
    }
}

/*
 * Analizamos las funciones date() y time()
 */

$fechaActual = time();

mostrar("Rato, raro: ", $fechaActual);
mostrar(date("d/m/y", $fechaActual));

mostrar(date("d/m/y"));
mostrar(date("j"), date("d"), date("D"));
mostrar(date("N"), date("F"), date("m"));
mostrar(date("Y"), date("Y"));
?>
