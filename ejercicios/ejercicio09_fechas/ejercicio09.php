<!DOCTYPE html>
<?php
$errores = [];
if ($_REQUEST) {
    $inicio = $_REQUEST['fechai'];
    $fin = $_REQUEST['fechaf'];

    $inicioArray = explode("/", $inicio);
    if (!@checkdate($inicioArray[1], $inicioArray[0], $inicioArray[2])) {
        $errores[] = "La fecha de inicio no es correcta";
    }

    $finArray = explode("/", $fin);
    if (!@checkdate($finArray[1], $finArray[0], $finArray[2])) {
        $errores[] = "La fecha de fin no es correcta";
    }

    if (!count($errores)) {
        $i = strtotime(implode("/", array_reverse($inicioArray)));
        $f = strtotime(implode("/", array_reverse($finArray)));
        if ($i <= $f) {
            $d = floor(abs(($f - $i) / (24 * 60 * 60)));
            mostrar_resultado($d);
        } else {
            $errores[] = "La fecha de fin debe ser mayor que la de inicio";
        }
    }
    mostrar_formulario($errores, $inicio, $fin);
} else {
    mostrar_formulario($errores);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            *{margin: 0px; padding: 0px;}
            .errores{width: 200px; min-height: 100px; margin: 10px auto; border: 1px solid crimson; color: tomato; }
            .salida{width: 200px; height: 100px; margin: 10px auto; border: 1px solid #CCC; color: #33ccff; text-align: center; line-height: 100px; }
            form{width: 500px; margin: 50px auto;}
            label:hover::after{content: "-Ten cuidado con los formatos";}
            input{display: block; margin-bottom: 10px;}
        </style>
    </head>
    <body>
        <?php

        function mostrar_formulario($errores, $inicio = "", $fin = "") {
            if (count($errores)) {
                echo '<div class="errores">';
                foreach ($errores as $error) {
                    echo "$error <br>";
                }
                echo'</div>';
            }
            ?>
            <form method="get">
                <label for="fechai">Introduce fecha inicial</label>
                <input type="date" id="fechai" required="true" placeholder="dd/mm/yyyy" value="<?= $inicio ?>"/>
                <label for="fechaf">Introduce fecha fin</label>
                <input type="date" id="fechaf" required="true" placeholder="dd/mm/yyyy" value="<?= $fin ?>"/>
                <input type="submit" value="Calcular"/>
            </form>
            <?php
        }

        function mostrar_resultado($d) {
            echo '<div class="salida">';
            echo $d;
            echo '</div>';
        }
        ?>

    </body>
</html>
