<?php

function mostrar() {
    static $c = 0;
    foreach (func_get_args() as $valor) {
        echo "<br> $c-$valor<br>";
        $c++;
    }
}

setlocale(LC_ALL, 'spanish');
mostrar(strftime("%A/%B"));
mostrar(strftime("%A", mktime(0, 0, 0, 1, 1, 1901)));
