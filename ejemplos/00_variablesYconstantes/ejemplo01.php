<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         $a = 10;
         echo gettype($a);
         unset($a);
        ?>
        <?php
         $variable1 = TRUE;
         echo "<p>" . gettype($variable1);
         settype($variable1, "integer");
         echo "<p>" . gettype($variable1);
        ?>
        <?php
        // variable entera;
        $a = 0;
        //variable real;
        $b = 0.0;
        //variable string;
        $c = "";
        //variable array
        $d = [2,3,4,5];
        
        echo "<div>La variable \$a: vale $a y es de tipo " . gettype($a) . "</div>";
        echo "<div>Toda la información sobre la variable</div>";
        var_dump($a);
        
        echo "<div>La variable \$b: vale $b y es de tipo " . gettype($b) . "</div>";
        echo "<div>Toda la información sobre la variable</div>";
        var_dump($b);
        
        echo "<div>La variable \$c: vale $c y es de tipo " . gettype($c) . "</div>";
        echo "<div>Toda la información sobre la variable</div>";
        var_dump($c);
        
        echo "<div>La variable \$d: vale $d y es de tipo " . gettype($d) . "</div>";
        echo "<div>Toda la información sobre la variable</div>";
        var_dump($d);        
        
        ?>      
        
        <?php
        // variable string;
        $variable_1 = "Normal";
        //variable string con formato heredoc;
        $variable_2 = <<<EOT
                    Esto es texto en formato
                        de varias lineas <br>
                            Formato heredoc.
EOT;
        
        $variable_3 = <<<EOO
                    Esto es texto en formato
                        de varias lineas <br>
                            Formato heredoc.
EOO;
        
        echo $variable_1;
        ?>
        <div>
            <?php
                echo $variable_2;    
            ?>
        </div>

            <?php
                echo $variable_3;    
            ?>         
    </body>
</html>
