<?php

/**
 * Description of Frase
 *
 * @author ramon
 */
class Frase extends Palabra{
    
    
    public $palabras;
    public function __construct($valor) {
        parent::__construct($valor);
        $this->calcularPalabras();
    }
    


    public function getPalabras() {
        return $this->palabras;
    }


    public function calcularPalabras() {
        $this->palabras=count(explode(" ",$this->getValor()));
    }



}
