<?php

/**
 * Description of Palabra
 *
 * @author ramon
 */
class Palabra {
    public $valor;
    public $vocales;
    public $caracteres;
    
    public function __construct($valor) {
        $this->valor = $valor;
        $this->calcularCaracteres();
        $this->calcularVocales();
    }
    
    public function getVocales() {
        return $this->vocales;
    }

    public function getCaracteres() {
        return $this->caracteres;
    }

    public function calcularVocales() {
        for($c=0,$this->vocales=0;$c<$this->getCaracteres();$c++){
            switch (strtolower($this->getValor()[$c])){
                case 'a':
                case 'e': 
                case 'i': 
                case 'o': 
                case 'u':
                    $this->vocales++;
            }
        }
    }

    public function calcularCaracteres() {
        $this->caracteres= strlen($this->getValor());
    }
    
    public function getValor() {
        return $this->valor;
    }

    public function setValor($valor) {
        $this->valor = $valor;
        //$this->caracteres= strlen($this->getValor());
        $this->calcularCaracteres();
        $this->calcularVocales();
        return $this;
    }


}
