<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $objetoPapa=new Palabra("ejemplo");
            echo $objetoPapa->getCaracteres() . "<br>";
            $objetoPapa->setValor("otro");
            echo $objetoPapa->getCaracteres() . "<br>";
            echo $objetoPapa->getVocales() . "<br>";
            $objetoHijo=new Frase("Esto es un ejemplo");
            echo $objetoHijo->getCaracteres(). "<br>";
            echo $objetoHijo->getVocales(). "<br>";
            echo $objetoHijo->getPalabras();
            
        ?>
    </body>
</html>
