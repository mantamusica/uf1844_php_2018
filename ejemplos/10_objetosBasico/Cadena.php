<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cadena
 *
 * @author Silvia
 */
class Cadena {
    #Propiedad pública
    public $texto;

    #Métodos públicos
    
    function getTexto() {
        return $this->texto;
    }

    function setTexto($texto) {
        $this->texto =  strtolower($texto);
    }

        
    
    public function caracterInicio() {
        //recomendable usar el get, que para eso lo tenemos
        #return $this->getTexto()[0]; //mejor opción
        #return substr($this->getTexto(),0,1);
        return $this->texto[0];
    }

    public function caracterFinal() {
        # $a=$this->getTexto();
        # return $a[strlen($a)-1];
        // return substr($this->getTexto(), -1); //mejor opción
        $num = strlen($this->texto);
        return $this->texto[$num - 1];
    }

    public function longitudVocales() {
        $cad = str_split($this->getTexto(), 1);

        for ($c = 0, $vocales = 0; $c < count($cad); $c++) {
            switch ($cad[$c]) {
                case "a":
                case "e":
                case "i":
                case "o":
                case "u":
                    $vocales++;
                    break;
            }
        }

        echo "<br> hay " . $vocales . " vocales ";
    }

    #Constructor
    function __construct($arg="") {
        $this->setTexto($arg);
    }

}
