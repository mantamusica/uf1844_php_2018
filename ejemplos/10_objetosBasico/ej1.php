<?php
    # Cargar clase
    require_once 'Cosa.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        #No hace falta instanciar clase para usar sus métodos estáticos
        echo Cosa::$fondo; //miembro de la clase
        // Instanciar clase 
        $objeto=new Cosa("rojo", "alto", 200, 180);
        #echo "<br>" . Cosa::$color;
         echo "<br>" . $objeto->color . "<br>"; //miembro del objeto
         echo $objeto->getAltura();
        echo "<br>"; 
        
        $objeto1=new Cosa("azul", "alto", 200, 180);
       echo $objeto1::$fondo; //miembro de la clase
       echo "<br>"; 
       echo $objeto::$fondo;
       Cosa::setFondo(45);
       echo "<br>"; 
       echo Cosa::$fondo;
       echo "<br>"; 
       echo $objeto::$fondo;
       echo "<br>"; 
       echo $objeto1::$fondo;
        
        ?>
    </body>
</html>
