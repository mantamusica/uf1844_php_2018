<?php
namespace uno;

function sumar() {
    return __NAMESPACE__ . " sumar_uno";
}

function restar() {
    return __NAMESPACE__ . " restar_uno";
}

namespace dos;

function sumar() {
    return __NAMESPACE__ .  " sumar_dos";
}

function restar() {
    return __NAMESPACE__ . " restar_dos";
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
    echo sumar() . "<br>";

    namespace tres;
    
    use function \uno\sumar; //importamos la funcion 
    echo sumar() . "<br>";
    // echo restar();  //error no existe la funcion
    
    function multiplicar(){
        return __NAMESPACE__ . " multiplicar";
    }
    
    echo multiplicar();
    

?>
    </body>
</html>
