<?php
require 'mostrar.php';
/**
  * ordenar arrays
  */
 $frutas1 = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana'
    ];
 
  $frutas2 = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana'
    ];
  $frutas3=$frutas1;
  $frutas4=$frutas1;
  $frutas5=$frutas1;
  $frutas6=$frutas1;
  
  
 // funcion que mantiene la posicion de los indices
 // Esta función se utiliza principalmente para ordenar arrays asociativos en los que el orden es importante.
    asort($frutas1); // orden ascendente
    arsort($frutas2); // orden descendente
 
// funcion que no mantiene los indices
    sort($frutas3); // orden ascendente
    rsort($frutas4); // orden descendente

// funcion que ordena por los indices (mantiene la coherencia entre los indices y los valores)
    ksort($frutas5);
    krsort($frutas6);

    
$frutas7 = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta13' => 'uva',
    'fruta9' => 'manzana1',
    'fruta25' => 'manzana2'
    ];

$frutas8=$frutas7;
$frutas9=array_flip($frutas7); 
$frutas10=$frutas9;
$frutas11=$frutas9;
$frutas12=$frutas9;

    
// Ordenes naturales

// orden por indices
   ksort($frutas7); // ordenacion normal
   ksort($frutas8,SORT_NATURAL); //ordenacion natural

// orden por valores
   asort($frutas9); // ordenacion normal
   natsort($frutas10); // ordenacion natural

// orden por valores sin coherencia de indices
   sort($frutas11); //ordenacion normal
   sort($frutas12,SORT_NATURAL); //ordenacion natural

 
mostrarTodo(get_defined_vars());