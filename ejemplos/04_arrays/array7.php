<?php
require 'mostrar.php';
 /**
  * Eliminar los elementos de un array
  */
 
 $borrar=[1,2,3,4,5,6,7,8];
 $borrar1=array_pop($borrar); // borra un elemento del final del array
 $borrar2= array_shift($borrar); // borra un elemento del principio del array
 $borrarA=[1,2,3,4,5,6,7];
 $borrarB=[
     'uno'=>10,
     'dos'=>20,
     'tres'=>30,
     'cuatro'=>40
 ];
 
 // elimina un elemento del array manteniendo los indices
 unset($borrarA[2]);
 unset($borrarB['dos']);
 
 // elimina elementos de un array perdiendo los indices
 
  $borrarA1=[1,2,3,4,5,6,7];
 $borrarB1=[
     'uno'=>10,
     'dos'=>20,
     'tres'=>30,
     'cuatro'=>40
 ];
 array_splice($borrarA1,3,2);
 array_splice($borrarB1,2,1); //puede borrar array asociativos
 
 
  
 // funcion de filtrado de elementos
 $borrar3= array_filter($borrar,
         function($v){
          if($v%2){
             return TRUE;
          } else {                                                        return FALSE;
          }
        }); // elimina los elementos del array si la funcion devuelve false
 
 $borrar4=array_filter([
                            'uno'=>1,
                            'dos'=>2,
                            'tres'=>3
                        ],function($v,$i){
                           if(strlen($i)<4 && $v<2){
                              return TRUE;
                           }else{
                              return FALSE;
                           }
                         },ARRAY_FILTER_USE_BOTH); // podemos pasar la clave y el valor a array_filter
 
                         
                         $minimo=5;
 $borrar5=array_filter($borrar,function($v)use($minimo){
                         if($v>=$minimo){
                          return TRUE;
                         }  
          }); //podemos utilizar la instruccion use para pasar un argumento a la funcion anonima
 

  // elimina elementos repetidos
                                                        
  $repetidos=[1,"uno"=>1,"dos"=>2,1,2,3,1,4,2,2,2,3,4,5,6,7];
  
  $sinRepetidos= array_unique($repetidos);
 

mostrarTodo(get_defined_vars());