<?php
require 'mostrar.php';
 /**
  * Operadores de un array
  */
 
 // unir
 $o1=[1,2,3];
 $o2=[4,5,6,7];
 
 $o3=$o1+$o2;
 $o4=$o2+$o1; // no es conmutativa
 
 $o11=[
     'a'=>10,
     'b'=>11,
     12
 ];

 $o12=[
     21,
     'a'=>22
 ];
 
 $o13=$o11+$o12;
 $o14=$o12+$o11; // tiene prioridad el de la izquierda
 
// comparar

$o20=[1,2,3];
$o21=[1,2,3];
$o22=[2,1,3];
$o23=[0=>1,2=>3,1=>2];

echo "<br>";
echo '$o20==$o21==>' . (int)($o20==$o21);
echo "<br>";
echo '$o20==$o22==>' . (int)($o20==$o22);
echo "<br>";
echo '$o23==$o20==>' . (int)($o23==$o20);
echo "<br>";
echo '$o23===$o20==>' . (int)($o23===$o20); // comprueba que esten en el mismo orden
echo "<br>";

mostrarTodo(get_defined_vars());