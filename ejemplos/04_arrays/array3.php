<?php
require 'mostrar.php';

$v6=array(
    "va1"=>10,
    "va2"=>"ww",
    "va3"=>13
);

$v7=[
    "va1"=>10,
    "va2"=>"ww",
    "va3"=>13
];

$v8=[
     0=>["a","b"],
     "vv1"=>[1,2,3,4],
 ];

/**
 * Leer un array
 */

// un solo valor
echo "<br>";
echo '$v7["va1"]=>' . $v7['va1'];
echo "<br>";

//todos los valores
echo "<br>";
foreach($v6 as $i=>$v){
    echo "<br>$i=>$v";
}
echo "<br>";


// todos los valores en un array bidimensional
echo "<br>";
foreach($v8 as $i=>$v){
    echo "<br>";
    if(is_array($v)){
        echo "array " . $i; 
        foreach($v as $i1=>$v1){
            echo "<br>$i1=>$v1";
        }
    }else{
            echo "<br>$i=>$v";
    }
    
}
echo "<br>";

// los valores de un indice

$baseDatos=[
  [
      'nombre'=>'ramon',
      'edad'=>50,
      'email'=>'r@r.es'
  ],
  [
      'nombre'=>'rosa',
      'edad'=>23,
      'email'=>'r@r.es'
  ],
  [
      'nombre'=>'jose',
      'edad'=>32,
      'email'=>'r@r.es'
  ]  
];

$nombres=array_column($baseDatos, "nombre"); //crea un array con los nombres



mostrarTodo(get_defined_vars());