<?php
require 'mostrar.php';
/**
  * Funciones especiales con arrays
  */

   /**
    * array_count_values
    */
   
   // contar repeticiones de un array (frecuencia)
   $contar=[
       'uno'=>1,
       1,20,3,4,1,1,14,1,1,1,
       'dos'=>2,
       'tres'=>3,
       2,2,2,2,2,2,2,10
   ];
   
   $contarValores= array_count_values($contar);
   
   /**
    * devolver valores
    */
   
   $todosValores= array_values($contarValores);
   
   
   /**
    * Devolver claves
    */
   
   $todosIndices= array_keys($contarValores);
   
   /**
    * Buscar valores
    */
   $buscar1=[1,2,3,1,2,3,12,3,12];
   
   $todosIndicesValor= array_keys($buscar1,2); // busca un valor y te indica en que indices lo ha encontrado
   $indiceValor= array_search(2,$buscar1); // buscar un valor y te indica el primer indice donde lo ha encontrado
 
   /**
    * Comprobar si existe una clave o valor
    */
   $buscar2=[
     'fruta1'=>'naranja',
     'fruta2'=>'manzana',
     'fruta3'=>'naranja'
   ];
   
   $existeValor= in_array('naranja',$buscar2);
   $existeIndice= array_key_exists('fruta1', $buscar2);
   
   /**
    * convierte un array en string
    */
   
  $unirCadena=[
  [
      'nombre'=>'ramon',
      'edad'=>50,
      'email'=>'r@r.es'
  ],
  [
      'nombre'=>'rosa',
      'edad'=>23,
      'email'=>'r@r.es'
  ],
  [
      'nombre'=>'jose',
      'edad'=>32,
      'email'=>'r@r.es'
  ]  
];
   
   $cadena=implode(",",$buscar2);
   //$cadena1=implode(",",$unirCadena); // no podemos utilizarlo con arrays bidimensionales
   
   /**
    * Selecciona elementos aleatorios de un array
    */
   
    $frutasAleatorio = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana'
    ];
   
    $aleatorio= array_rand($frutasAleatorio); // devuelve un indice de forma aleatoria
    $aleatorio2= array_rand($frutasAleatorio,3);
    $aleatorio3=array_rand($unirCadena); // un elemento de la primera dimension
    
    /**
     * Extrae un trozo del array
     */
    
    //indicamos los elementos a quedarnos
    $borrarA2=[1,2,3,4,5,6,7];
    $borrarB2=[
     'uno'=>10,
     'dos'=>20,
     'tres'=>30,
     'cuatro'=>40
    ];
 
    $borrarA21=array_slice($borrarA2, 3,2); //no mantiene los indices
    $borrarA22=array_slice($borrarA2, 3,2,TRUE); //mantiene los indices
    $borrarB21=array_slice($borrarB2,1,2); // mantiene los indices al ser asociativo
    
    /**
     * Aplica una operacion a los elementos de los arrays dados
     */
    
    $uno=[1,2,3,4,5];
    $dos=[10,10,10,20,30,40];
    $tres=[
    [
        'nombre'=>'ramon',
        'edad'=>50,
        'email'=>'r@r.es'
    ],
    [
        'nombre'=>'rosa',
        'edad'=>23,
        'email'=>'r@r.es'
    ],
    [
        'nombre'=>'jose',
        'edad'=>32,
        'email'=>'r@r.es'
    ]  
    ];
    $cinco=$tres;
    
    $cuatro=$uno;
    $seis=$tres;
    $walkSalida="";
    $salida="";
    
    $array_map=array_map(function($a,$b){return $a*$b;},$uno,$dos);
    
    /**
     * Aplica una funcion a los elementos del array pasado, permite pasar argumentos
     * No permite pasar argumentos por referencia
     * para ello tienes que utilizar use
     */
    
    array_walk($cuatro, function(&$v,$i,$a){$v=$a+$v+$i;},10);
    array_walk($tres,function(&$v,$i){$v=implode(",",$v);});
    array_walk($seis,function($v)use(&$salida){$salida.=implode(",",$v) .",";});

    // para arrays multidimensionales
    array_walk_recursive($cinco,function($v,$i){ 
        if(!is_array($v)){
            echo $v;
        }
    }); // no es necesario los valores que sean arrays no se pasan
    array_walk_recursive($cinco,function($v,$i){echo $v;}); // hace lo mismo que la anterior
    array_walk_recursive($cinco,function($v,$i)use(&$walkSalida){ $walkSalida.=$v . ",";}); // hace lo mismo que la anterior
    
    
    

 
mostrarTodo(get_defined_vars());