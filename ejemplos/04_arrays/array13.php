<?php

$a=[
    'nombre'=>'Ramon',
    'Apellidos'=>'Abramo',
    'edad'=>40,
    'telefono'=>'942942942'
];

function eliminar(&$vector,$indices){
    foreach($indices as $clave){
        $vector=array_filter($vector,function($valor)use($clave){
            if($valor==$clave){
                return false;
            }else{
                return true;
            }
        },ARRAY_FILTER_USE_KEY);
    }
}

function eliminar1(&$vector,$indices){
    foreach($vector as $indice=>$valor){
        foreach($indices as $clave){
            if($clave==$indice){
                unset($vector[$indice]);
                break;
            }
        }
    }
    
}

function eliminar2(&$vector,$indices){
    $aux=$vector;
    $vector=[];
    foreach($aux as $indice=>$valor){
        $llave=0;
        foreach($indices as $clave){
            if($clave==$indice){
                $llave=1;
            }
        }
        if(!$llave){
            $vector[$indice]=$valor;
        }
    }
}

function eliminar3(&$vector,$indices){
    $aux=array_flip($vector);
    $vector=array_flip(array_diff($aux, $indices));
}

eliminar($a,['nombre','telefoo']);
var_dump($a);


