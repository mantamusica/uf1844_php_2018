<?php
require 'mostrar.php';
/**
  * Unir arrays
  */
 
 // combina dos arrays uno hace de claves y otro de valores
 $claves=['nombre','edad','tel'];
 $valores=['Ramon',44,'987987987'];
 $auto6= array_combine($claves, $valores);
 
 // combina arrays
 
 $array1    = array("color" => "red", 2, 4);
 $array2    = array("a", "b", "color" => "green", "forma" => "trapezoide", 4);
 $resultado = array_merge($array1, $array2);
 
 // combina arrays con operador
 
 $resultado1=$array1+$array2;
 $resultado2=$array2+$array1; // no es conmutativa
 
mostrarTodo(get_defined_vars());