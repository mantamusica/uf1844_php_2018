<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
function mostrarCadena()
{
        $cadena = "Cadena local";
        echo "Cadena 2 = ".$cadena."<br>";
}

$cadena = "Cadena global";
echo "Cadena 1 = ".$cadena."<br />";

mostrarCadena();

// el resultado de este script será el siguiente
// Cadena 1 = Cadena global
// Cadena 2 = Cadena local

?>
    </body>
</html>
