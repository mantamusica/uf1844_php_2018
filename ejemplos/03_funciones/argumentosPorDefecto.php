<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
    <?php
    function verPeliculas($titulos = array("Avatar"), $genero = null){
        $generoPelicula = is_null($genero) ? "" : " cuyo género es: $genero";
            return "Hoy vamos a ver: ".join(", ", $titulos) . $generoPelicula . "<br>";
    }
    
    echo verPeliculas();
    echo verPeliculas(array("La jungla de cristal", "007", "Transporter"), "acción");
    ?>
    </body>
</html>
